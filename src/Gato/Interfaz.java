/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Gato;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author vane_
 */
public class Interfaz extends JFrame implements ActionListener {

    JButton play;
    JButton panel[][];
    String player1, player2;
    int turno = -1;
    Color c1;

    public Interfaz() {
        this.setLayout(null);
        play = new JButton("INICIAR JUEGO");
        play.setBounds(175, 350, 150, 30);
        play.addActionListener(this);
        this.add(play);
        panel = new JButton[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                panel[i][j] = new JButton();
                panel[i][j].setBounds((i + 1) * 80 + 40, (j + 1) * 80, 80, 90);
                this.add(panel[i][j]);
                panel[i][j].addActionListener(this);
            }
        }
        c1 = panel[0][0].getBackground();
    }



    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == play) {
            turno = 0;
            player1 = JOptionPane.showInputDialog("Ingresa nombre del primer jugador");
            player2 = JOptionPane.showInputDialog("Ingresa nombre del segundo jugador");
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    panel[i][j].setEnabled(true);
                    panel[i][j].setText("");
                    panel[i][j].setBackground(c1);
                }
            }
        } else {
            JButton boton = (JButton) e.getSource();
            if (turno == 0) {
                if (boton.getText().equals("")) {
                    boton.setBackground(Color.ORANGE);
                    boton.setText("X");
                    boton.setEnabled(false);
                    turno = 1;
                }
            } else {
                if (turno == 1) {
                    if (boton.getText().equals("")) {
                        boton.setBackground(Color.LIGHT_GRAY);
                        boton.setText("O");
                        boton.setEnabled(false);
                        turno = 0;
                    }
                }
            }
            Estado();
        }
    }

    public void Estado() {
        int ganador = 0;
        for (int i = 0; i < 3; i++) {
            if (panel[0][i].getText().equals("X") && panel[1][i].getText().equals("X")
                    && panel[2][i].getText().equals("X")) {
                ganador = 1;
            }
            if (panel[i][0].getText().equals("X") && panel[i][1].getText().equals("X")
                    && panel[i][2].getText().equals("X")) {
                ganador = 1;
            }
        }
        if (panel[0][0].getText().equals("X") && panel[1][1].getText().equals("X")
                && panel[2][2].getText().equals("X")) {
            ganador = 1;
        }
        if (panel[0][2].getText().equals("X") && panel[1][1].getText().equals("X")
                && panel[2][0].getText().equals("X")) {
            ganador = 1;
        }
        for (int i = 0; i < 3; i++) {
            if (panel[0][i].getText().equals("O") && panel[1][i].getText().equals("O")
                    && panel[2][i].getText().equals("O")) {
                ganador = 1;
            }
            if (panel[i][0].getText().equals("O") && panel[i][1].getText().equals("O")
                    && panel[i][2].getText().equals("O")) {
                ganador = 1;
            }
        }
        if (panel[0][0].getText().equals("O") && panel[1][1].getText().equals("O")
                && panel[2][2].getText().equals("O")) {
            ganador = 1;
        }
        if (panel[0][2].getText().equals("O") && panel[1][1].getText().equals("O")
                && panel[2][0].getText().equals("O")) {
            ganador = 1;
        }
        if (ganador == 1) {
            JOptionPane.showMessageDialog(this, "Felicidades " + player1);
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    panel[i][j].setEnabled(false);
                }
            }
        }
        if (ganador == 2) {
            JOptionPane.showMessageDialog(this, "Felicidades " + player2);
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    panel[i][j].setEnabled(false);
                }
            }
        }
    }

    public static void main(String[] args) {
        Interfaz ventana = new Interfaz();
        ventana.setDefaultCloseOperation(3);
        ventana.setSize(500, 500);
        ventana.setLocationRelativeTo(null);
        ventana.setResizable(false);
        ventana.setTitle("GATO");
        ventana.setVisible(true);
    }
}
