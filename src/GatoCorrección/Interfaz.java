/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GatoCorrección;

import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author vane_
 */
public class Interfaz extends javax.swing.JFrame {
    
    char[][] matriz;
    boolean contraMaquina = true;
    int nivel = 9;
    char turno = 'X';

    /**
     * Creates new form Interfaz
     */
    public Interfaz() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        Maquina = new javax.swing.JButton();
        DosJugadores = new javax.swing.JButton();
        L1 = new javax.swing.JButton();
        L2 = new javax.swing.JButton();
        L3 = new javax.swing.JButton();
        L8 = new javax.swing.JButton();
        L5 = new javax.swing.JButton();
        L6 = new javax.swing.JButton();
        L9 = new javax.swing.JButton();
        L4 = new javax.swing.JButton();
        L7 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Maquina.setText("MAQUINA");
        Maquina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MaquinaActionPerformed(evt);
            }
        });

        DosJugadores.setText("DOS JUGADORES");
        DosJugadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DosJugadoresActionPerformed(evt);
            }
        });

        L1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L1ActionPerformed(evt);
            }
        });

        L2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L2ActionPerformed(evt);
            }
        });

        L3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L3ActionPerformed(evt);
            }
        });

        L8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L8ActionPerformed(evt);
            }
        });

        L5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L5ActionPerformed(evt);
            }
        });

        L6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L6ActionPerformed(evt);
            }
        });

        L9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L9ActionPerformed(evt);
            }
        });

        L4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L4ActionPerformed(evt);
            }
        });

        L7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                L7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(L4, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(L7, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(L5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(L6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(L8, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(L9, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(L1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(L2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(L3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(Maquina)
                                .addGap(18, 18, 18)
                                .addComponent(DosJugadores)))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(L3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L2, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(L5, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L6, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L4, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(L7, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L9, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(L8, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DosJugadores)
                    .addComponent(Maquina))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        L2.getAccessibleContext().setAccessibleDescription("");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void L1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L1ActionPerformed
        // TODO add your handling code here:
        L1.setText(turno + "");
        matriz[0][0] = turno;
        L1.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L1ActionPerformed

    private void L2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L2ActionPerformed
        // TODO add your handling code here:
                L2.setText(turno + "");
        matriz[0][1] = turno;
        L2.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L2ActionPerformed

    private void L3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L3ActionPerformed
        // TODO add your handling code here:
                L3.setText(turno + "");
        matriz[0][2] = turno;
        L3.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L3ActionPerformed

    private void L4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L4ActionPerformed
        // TODO add your handling code here:
                L4.setText(turno + "");
        matriz[1][0] = turno;
        L4.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L4ActionPerformed

    private void L5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L5ActionPerformed
        // TODO add your handling code here:
                L5.setText(turno + "");
        matriz[1][1] = turno;
        L5.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L5ActionPerformed

    private void L6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L6ActionPerformed
        // TODO add your handling code here:
                L6.setText(turno + "");
        matriz[1][2] = turno;
        L6.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L6ActionPerformed

    private void L7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L7ActionPerformed
        // TODO add your handling code here:
                L7.setText(turno + "");
        matriz[2][0] = turno;
        L7.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L7ActionPerformed

    private void L8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L8ActionPerformed
        // TODO add your handling code here:
                L8.setText(turno + "");
        matriz[2][1] = turno;
        L8.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L8ActionPerformed

    private void L9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_L9ActionPerformed
        // TODO add your handling code here:
                L9.setText(turno + "");
        matriz[2][2] = turno;
        L9.setEnabled(false);
        Turno();
    }//GEN-LAST:event_L9ActionPerformed

    private void MaquinaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MaquinaActionPerformed
        // TODO add your handling code here:
        DosJugadores.setEnabled(false);
        Iniciar(true);
    }//GEN-LAST:event_MaquinaActionPerformed

    private void DosJugadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DosJugadoresActionPerformed
        // TODO add your handling code here:
        DosJugadores.setEnabled(false);
        contraMaquina = false;
        Iniciar(true);
    }//GEN-LAST:event_DosJugadoresActionPerformed

       
    private boolean Correcto (char bueno) {
        boolean turn = false;
        if(matriz[0][0] == bueno && matriz[0][1] == bueno && matriz[0][2] == bueno) {
            turn = true;
        }else if (matriz[1][0] == bueno && matriz[1][1] == bueno && matriz[1][2] == bueno) {
            turn = true;
        }else if (matriz[2][0] == bueno && matriz[2][1] == bueno && matriz[2][2] == bueno) {
            turn = true;
            //Columnas
        }else if (matriz[0][0] == bueno && matriz[1][0] == bueno && matriz[2][0] == bueno) {
            turn = true;
        }else if (matriz[0][1] == bueno && matriz[1][1] == bueno && matriz[2][1] == bueno) {
            turn = true;
        }else if (matriz[0][2] == bueno && matriz[1][2] == bueno && matriz[2][2] == bueno) {
            turn = true;
            //Diagonales
        }else if (matriz[0][0] == bueno && matriz[1][1] == bueno && matriz[2][2] == bueno) {
            turn = true;
        }else if (matriz[0][2] == bueno && matriz[1][1] == bueno && matriz[0][2] == bueno) {
            turn = true;            
        } return turn;
    }
    
    private void Final(char ganador) {
        JOptionPane.showMessageDialog(null, "Felicidades " + ganador);
        DosJugadores.setEnabled(true);
        Maquina.setEnabled(true);
        nivel = 9;
        Iniciar(false);
    }
    
     private void Maquina() {
        boolean turn = true;
        int vert = 0, hori = 0;
        if (nivel > 1) {
            while (turn) {
                Random p1 = new Random();
                vert = (int) (p1.nextDouble() * 3 + 0);
                hori = (int) (p1.nextDouble() * 3 + 0);
                if (matriz[vert][hori] != 'X' && matriz[vert][hori] != 'O') {
                    matriz[vert][hori] = 'O';
                    turn = false;
                }
             }
         } else {
             for (int i = 0; i < 3; i++) {
                 for (int j = 0; j < 3; j++) {
                     if (matriz[i][j] != 'X' && matriz[i][j] != 'O') {
                         vert = i;
                         hori = j;
                         matriz[vert][hori] = 'O';
                     }
                 }
             }
         }
        String elemento = "" + vert + "" + hori;
        switch (elemento) {
            case "00":
                L1.setText("O");
                L1.setEnabled(false);
                break;
            case "01":
                L2.setText("O");
                L2.setEnabled(false);
                break;
            case "02":
                L3.setText("O");
                L3.setEnabled(false);
                break;
            case "10":
                L4.setText("O");
                L4.setEnabled(false);
                break;
            case "11":
                L5.setText("O");
                L5.setEnabled(false);
                break;
            case "12":
                L6.setText("O");
                L6.setEnabled(false);
                break;
            case "20":
                L7.setText("O");
                L7.setEnabled(false);
                break;
            case "21":
                L8.setText("O");
                L8.setEnabled(false);
                break;
            case "22":
                L9.setText("O");
                L9.setEnabled(false);
                break;            
        }
        nivel--;
        if (Correcto('O')) {
            Final('O');
        }
    }
    
    public void Iniciar(boolean select) {
        matriz = new char[3][3];
        L1.setEnabled(select);
        L2.setEnabled(select);
        L3.setEnabled(select);
        L4.setEnabled(select);
        L5.setEnabled(select);
        L6.setEnabled(select);
        L7.setEnabled(select);
        L8.setEnabled(select);
        L9.setEnabled(select);
        L1.setText("");
        L2.setText("");
        L3.setText("");
        L4.setText("");
        L5.setText("");
        L6.setText("");
        L7.setText("");
        L8.setText("");
        L9.setText("");
    }
    
    public void Turno() {
        nivel--;
        if (Correcto(turno)) {
            Final(turno);
        } else if (contraMaquina) {
            Maquina();
        } else {
            if (turno == 'X') {
                turno = 'O';
            } else {
                turno = 'X';
            }
        }
    }
    
    class Juego extends Thread {
        
        public boolean ejecutar = true;
        boolean modo;
        
        public Juego(boolean modo) {
            this.modo = modo;
        }
        
        @Override
        public void run() {
        }
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interfaz.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interfaz().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton DosJugadores;
    private javax.swing.JButton L1;
    private javax.swing.JButton L2;
    private javax.swing.JButton L3;
    private javax.swing.JButton L4;
    private javax.swing.JButton L5;
    private javax.swing.JButton L6;
    private javax.swing.JButton L7;
    private javax.swing.JButton L8;
    private javax.swing.JButton L9;
    private javax.swing.JButton Maquina;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
